<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = "posts";

    // mass assignment create()
    // protected $fillable = ["title", "value","user_id"];

    // guarded create()
    protected $guarded = [];

    public function author(){
    	return $this->belongsTo('App\User','user_id');
    }
    public function komen(){
    	return $this->hasMany('App\Comment','post_id');
    }
}
