<?php

namespace App\Exports;

use App\Post;
use Maatwebsite\Excel\Concerns\FromCollection;

class PostsExport implements FromCollection
{

    protected $id;

 function __construct($id) {
        $this->id = $id;
 }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return PostsExport::where($this->id)->get();
        ]);
    }
}
