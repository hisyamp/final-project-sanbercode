<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use App\Post;
// use App\Question;
use App\User;
use Auth;
use PDF;
// use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
        public function create(){
        	return view('posts.create');
    }

    public function store(Request $request){

        // menggunakan mass assignment create() 
        $question = Post::create([
            "nama_masakan" => $request["namaMasakan"],
            "deskripsi" => $request["deskripsi"],
            "bahan" => $request["bahan"],
            "langkah" => $request["langkah"],
            "user_id" => Auth::id()
        ]);

        Alert::success('Berhasil', 'Resep Berhasil Ditambahkan!');


    	return redirect('/posts/index')->with('success','Resep Telah Dibuat!');
    }
    public function index(){

        // menggunakan eloquen
        // $posts = Post::all();

        // $id = Auth::id();
        $user= Auth::user();
        $posts = $user->posts;
        // $posts = Post::where('id', $id)->get();
        // dd($posts);

        $user = User::all();
    	
    	return view('posts.index',compact('posts', 'user'));
    }
     public function show($id){
       
        // menggunakan eloquen
        $posts = Post::find($id);
        // $user = 
        
    	// dd($posts);
    	return view('posts/show',compact('posts'));
    }
    public function edit($id){
        
        // menggunakan eloquen
        $post = Post::find($id);
    	return view('posts.edit',compact('post'));
    }
    public function update($id,Request $request){
        
    	// dd($post);

        // menggunakan eloquen
        $update = Post::WHERE('id',$id)->update([
            "nama_masakan" => $request["namaMasakan"],
            "deskripsi" => $request["deskripsi"],
            "bahan" => $request["bahan"],
            "langkah" => $request["langkah"],
            "user_id" => Auth::id()

        ]);

        Alert::success('Berhasil', 'Resep Berhasil Diubah!');

    	return redirect('/posts/index')->with('success','Update Berhasil');
    	}
    public function destroy($id){

    	// dd($post);

           // menggunakan eloquen
        Post::destroy($id);
    return redirect('posts/index')->with('success','Pertanyaan Berhasil Dihapus! ');
    }
    public function showAll(){

        $user = Auth::user();
        $posts = Post::all();
    	
    	return view('posts.showAll',compact('posts', 'user'));
    }
    public function pdf($id){

        $posts = Post::find($id);
        // dd($posts);
        $pdf = PDF::loadView('print', compact('posts'));
        return $pdf->download('print.pdf');
        
    }

    public function excel(Request $request) 
    {
        return Excel::download(new Post($request->id), 'Posts.xlsx');
    }
    
};

