<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\User;
// use Auth;
use Illuminate\Support\Facades\Auth;

class ProfilController extends Controller
{
    public function show($id){
        // menggunakan query builder
    	// $post = DB::table('questions')->WHERE ('id', $id)->first();

        // menggunakan eloquen
        $post = User::find($id);
        $user = Auth::id();
    	// dd($post->question);
    	return view('posts.profile',compact('post','user'));
    }

    public function index(){

        // menggunakan eloquen
        // $posts = Post::all();

        $user = Auth::user();
        $posts = $user->posts;
        // dd($posts);

        // $user = User::all();
    	
    	return view('posts.index',compact('posts'));
    }
}