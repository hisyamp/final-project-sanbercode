<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Route;

Route::get('/', function () 
   { 
   	return view('auth.login');
   }
);
Route::get('/posts/create', 'PostController@create')->middleware('auth');
Route::post('/posts', 'PostController@store');
Route::get('/posts/index', 'PostController@index');
Route::get('posts/all' ,'PostController@showAll');
Route::get('/posts/{id}', 'PostController@show')->middleware('auth');
Route::get('/posts/{id}/edit', 'PostController@edit')->middleware('auth');
Route::put('/posts/{id}', 'PostController@update')->middleware('auth');
Route::delete('/posts/{id}', 'PostController@destroy')->middleware('auth');
Route::get('/profile/{id}', 'ProfilController@show')->middleware('auth');
Route::post('/comment/{id}', 'CommentController@store');
Route::delete('/comment/{id}', 'CommentController@destroy')->middleware('auth');
Route::get('posts/all' ,'PostController@showAll');

// domPDF
Route::get('/posts/printpdf/{id}' ,'PostController@pdf');

//excel laravel
Route::get('/posts/printexcel/{id}' ,'PostController@excel');

// Route::get(');
Route::get('/register', 'AuthController@register');
Route::post('/welcome', 'AuthController@welcome');

Route::get('/master',function(){
	return view('layouts.master');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
