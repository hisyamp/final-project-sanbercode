@extends('layouts.master')

@section('content')

<a href="/posts/create" class="btn btn-primary btn-sm ml">Let's share your tasty recipes </a>

        

<div class="card card-widget">
  <div class="card-header">
    <div class="user-block">
      <img class="img-circle" src="{{ asset('/adminlte/dist/img/AdminLTELogo.png') }}" alt="">
      <span class="username">{{ $user->name }}<a href=""></a></span>
      <span class="description">Shared publicly - Just Now</span>
    </div>
  </div>
</div>
<!-- The content-->
<section class="container row">
              <!-- /.card-header -->
@foreach($posts as $post)

  <div class="card col-sm-4">
    <div class="card-title bg-dark text-white container-fluid">
      <h4>{{ $post->nama_masakan}}</h4>
    </div>
    <div class="card-body">
        <p>{{ $post->deskripsi}}</p>
    </div>
    <div class="card card-footer">
      <a href="/posts/{{ $post->id }}" class="btn btn-primary">
        Show Recipe
      </a>

      <!-- <a href="/posts/{{$post->id}}/edit" class="btn btn-secondary">
        Edit
      </a>

      <form action="/posts/{{$post->id}}" method="POST" class="">
        @csrf
        @method('DELETE')
        <button  type="submit" class="btn btn-danger  container-fluid">
          Delete
        </button>
    </form> -->
    </div>
  </div>                
@endforeach
</section>

@endsection