@extends('layouts.master')

@section('content')
<section class="content">
<a href="/posts/create" class="btn btn-primary btn-sm mb-5 ml-0">Let's share your recipes</a>
</section>

<!--the contents is flled here-->
<section class="container row">
@foreach($posts as $post)
<div class="card col-sm-4 mb-2">
  <div class="card-header  bg-dark text-white">
    <h4>{{$post->nama_masakan}}</h4>
  </div>
  <div class="card-body">
    <p>{{$post->deskripsi}}</p>
  </div>
  <div class="card card-footer d-inline-flex ">
    <a href="/posts/{{ $post->id }}" class="btn btn-primary ">
      Show Recipe
    </a>
    <a href="/posts/{{ $post->id }}/edit" class="btn btn-secondary">
      Edit
    </a>
    <form action="/posts/{{$post->id}}" method="POST" >
        @csrf
        @method('DELETE')
        <input type="submit" value="Delete" class="btn btn-danger container-fluid">
    </form>
      
    
  </div>

</div>

@endforeach
</section>
@endsection