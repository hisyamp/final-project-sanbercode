@extends('layouts.master')
@section('content')

<section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md">

          <!-- Profile Image -->
          <div class="card card-primary card-outline">
            <div class="card-body box-profile">
              <div class="text-center">
                <img class="profile-user-img img-fluid img-circle" src="{{ asset('/adminlte/dist/img/AdminLTELogo.png') }}" alt="User profile picture">
              </div>

              <h3 class="profile-username text-center">{{ Auth::user()->name }}<a href=""></a></h3>

              <p class="text-muted text-center">Juru Masak</p>

              
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

          <!-- About Me Box -->
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">About Me</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <strong><i class="fas fa-book mr-1"></i> Education</strong>

              <p class="text-muted">
                D3 STP Bandung 
              </p>

              <hr>

              <strong><i class="fas fa-map-marker-alt mr-1"></i> Location</strong>

              <p class="text-muted">Bandung, Indonesia</p>

              <hr>

              <strong><i class="fas fa-pencil-alt mr-1"></i> Skills</strong>

              <p class="text-muted">
                <span class="tag tag-danger">Memasak (8/10)</span>
              </p>

              <hr>

              <strong><i class="far fa-file-alt mr-1"></i> Email</strong>

              <p class="text-muted">{{ Auth::user()->email }}</p>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>

@endsection