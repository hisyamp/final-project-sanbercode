@extends('layouts.master')

@section('content')
    
  <div class="ml-3 mt-3">
		<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Create Recipe</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/posts" method="post">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    
                  </div>
                  <div class="form-group">
                    <label for="title">Nama Masakan</label>
                    <input type="text" class="form-control" id="namaMasakan" name="namaMasakan" placeholder="Masukkan Nama Masakan">
                    @error('namaMasakan')
                      <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                  </div>
                  <div class="form-group">
                    <label for="title">Deskripsi</label>
                    <input type="text" class="form-control" id="deskripsi" name="deskripsi" placeholder="Deskripsi">
                    @error('deskripsi')
                      <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                  </div>
                  <div class="form-group">
                    <label for="question">Bahan</label>
                    <input type="text" class="form-control" id="bahan" name="bahan" placeholder="Nama Bahan Bahan">
                    @error('question')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="langkah">Langkah Pembuatan</label>
                    <input type="text" class="form-control" id="langkah" name="langkah" placeholder="Masukkan Langkah Pembuatan">
                    @error('langkah')
                      <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                  </div>
                  
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Create</button>
                </div>
              </form>
            </div>
  </div>
@endsection

<!-- 
@push('scripts')
<script>
  Swal.fire({
      title: 'Berhasil!',
      text: 'Memasangkan script sweet alert',
      icon: 'success',
      confirmButtonText: 'Cool'
  })
</script>
@endpush -->
