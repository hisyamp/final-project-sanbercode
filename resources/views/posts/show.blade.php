@extends('layouts.master')

@section('style')
<!-- <style> 
.container{
    background-color: #fff;
    padding: 10px;
    border-radius: 5px;
    border: 1px solid #d6d2c4;
    margin-right: 5px;
      }
.batas{
    border-bottom: 1px solid #d6d2c4;
    padding-bottom: 4px;
}
.answer{
    display: flex;
}

</style> -->
@endsection

@section('content')
<div class="card card-widget">
              <div class="card-header">
                <div class="user-block">
                  <img class="img-circle" src="{{ asset('/adminlte/dist/img/AdminLTELogo.png') }}" alt="">
                  <span class="username"><a href="#">{{ $posts->author->name}}</a></span>
                  <span class="description">Shared publicly - {{ $posts->created_at }}</span>
                </div>
                
                <!-- /.user-block -->
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-toggle="tooltip" title="Mark as read">
                    <i class="far fa-circle"></i></button>
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i>
                  </button>
                </div>
                <!-- /.card-tools -->
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <!-- post text -->
                <h4>{{ $posts->nama_masakan}}</h4>

                <p>{{ $posts->deskripsi}}</p>
                <p>{{ $posts->bahan}}</p>
                <p>{{ $posts->langkah}}</p>

                
                <!-- Social sharing buttons -->
                <a href="/posts/printpdf/{{ $posts->id }}"><button type="button" class="btn btn-default btn-sm"><i class="fas fa-print"></i> Print Recipe</button></a>
                <a href="/posts/printexcel/{{ $posts->id }}"><button type="button" class="btn btn-default btn-sm"><i class="fas fa-download"></i> Export to Excel</button></a>
                <span class="float-right text-muted">45 likes - 2 comments</span>
              </div>
              <!-- /.card-body -->
              @foreach ( $posts->komen as $komen)   
              <div class="card-footer card-comments" style="border-bottom: 1px solid #e1e1e1;">
                <div class="card-comment">
                  <!-- User image -->
                  <img class="img-circle img-sm" src="{{ asset('/adminlte/dist/img/AdminLTELogo.png') }}" alt="">
                  <div class="comment-text">
                    <span class="username">
                      {{$komen->profil->name}}
                      <form action="/comment/{id}" method="POST" >
                          @csrf
                          @method('DELETE')
                     <button type="button" class="btn btn-tool float-right" data-card-widget="remove"><i class="fas fa-times"></i>
                  </button>
                     </form>
                      
                      <span class="text-muted float-right">{{ $komen->created_at }}</span>
                    </span><!-- /.username -->
                  </div>
                  <!-- /.comment-text -->
                      <p>{{ $komen->value}}</p>
                </div>
            </div>  
              @endforeach
                                
              <div class="card-footer">
                <form action="/comment/{{ $posts->id }}" method="post">
                 @csrf
                  <!-- <img class="img-fluid img-circle img-sm" src="../dist/img/user4-128x128.jpg" alt=""> -->
                  <!-- .img-push is used to add margin to elements next to floating images -->
                  <div class="img-push" style="display: flex;">
                    <input type="text" id="comment" name="comment" class="form-control form-control-sm" placeholder="Press enter to post comment">
                    <button type="submit" class="btn btn-primary btn-sm">Create</button>
                  </div>
                </form>
              </div>
              <!-- /.card-footer -->
            </div>
   
@endsection