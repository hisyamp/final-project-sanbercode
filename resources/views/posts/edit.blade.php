@extends('layouts.master')

@section('content')
    
  <div class="ml-3 mt-3">
		<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Question</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/posts/{{$post->id}}" method="post">
                @csrf
                @method('PUT')
                <div class="ml-3 mt-3" mt>
                  
                <!-- <div class="card-body">
                  
                  </div> -->
                  <div class="form-group">
                    <label for="namaMasakan">Nama Masakan</label>
                    <input type="text" class="form-control" value=" {{ old('nama_masakan', $post->nama_masakan) }} " id="namaMasakan" name="namaMasakan" placeholder="Nama Masakan">
                    @error('namaMasakan')
                      <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                  </div>
                  <div class="form-group">
                    <label for="deskripsi">Deskripsi</label>
                    <input type="text" class="form-control" value="{{ old('deskripsi', $post->deskripsi) }}" id="deskripsi" name="deskripsi" placeholder="deskripsi">
                    @error('deskripsi')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                
                  <div class="form-group">
                    <label for="bahan">Bahan</label>
                    <input type="text" class="form-control" value="{{ old('bahan', $post->bahan) }}" id="bahan" name="bahan" placeholder="Bahan">
                    @error('bahan')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  
                  <div class="form-group">
                    <label for="langkah">Langkah</label>
                    <input type="text" class="form-control" value="{{ old('langkah', $post->langkah) }}" id="langkah" name="langkah" placeholder="langkah">
                    @error('langkah')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                </div>
                  
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Edit</button>
                </div>
              </form>
            </div>
  </div>
@endsection