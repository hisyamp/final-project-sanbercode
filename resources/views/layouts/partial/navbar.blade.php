<nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top mb-3">
  <div class="container">
    <a class="navbar-brand" href="#" style="font-family: 'Aladin', cursive; font-size: 1.5em;">
          ResepKita
        </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item active">
          <a class="nav-link" href='/posts/all'>Home Page</a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="/profile/{{Auth::user()->id}}">My Profile</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/posts/index">My Recipes</a>
        </li>
        <li class="nav-item">
          <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
            {{ Auth::user()->name }} <span class="caret"></span>
        </a>

        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="{{ route('logout') }}"
               onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                {{ __('Logout') }}
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </li>
      </ul>
    </div>
  </div>
</nav>